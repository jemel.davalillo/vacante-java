const express = require('express');
const app = express();
const cors = require("cors");

require("dotenv").config();
const { conexion, close_connection } = require('./database/db');



app.use( cors() );
app.use( express.json() );//Siempre debe ir antes de las rutas


conexion;

app.use( express.static('public') );
app.listen(5000, () => {
    console.log('puerto 5000');
});

//Rutas de entidades
app.use("/", require('./routes/usuarios'));

app.use("/productos", require('./routes/productos'));
app.use("/compras", require('./routes/compras'));
app.use("/historicos", require('./routes/historicos'));