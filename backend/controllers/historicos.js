const response = require("express");
const { conexion } = require('../database/db');

const getHistoricos  = async(req, res=response) => {
    const id_cliente = req.params.id_cliente;
    await conexion.query(`SELECT * FROM log_transacciones WHERE id_cliente = ${id_cliente}`, (error, result) => {
        if(error){
            throw error;
        }else{
            const str_historico=JSON.stringify(result);
            const json_historico = JSON.parse(str_historico);
            return res.status(200).json({
                "ok": true,
                "historicos":json_historico
            });
        }
    });
};

module.exports = {
    getHistoricos
}