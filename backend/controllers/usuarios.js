const response = require("express");
const { conexion } = require('../database/db');

const getUsuarios = async(req, res=reponse) => {
    await conexion.query('SELECT * FROM cliente', (error, results) => {
        if(error){
            console.log(error);
        }else{
            const str_cliente=JSON.stringify(results);
            const json_cliente = JSON.parse(str_cliente);

           return res.status(200).json({
            "ok": true,
            "clientes":json_cliente
           });
        }
    });
};

const actualizarSaldo = async(req, res = response) => {
    const id_usuario = req.body.id;
    const saldo = req.body.saldo;
    try {
        //Verificar usuario activo
        conexion.query(`SELECT * FROM cliente WHERE activo = 1 AND id = ${id_usuario}`, (error, results) => {
            if(error){
                throw error;
            }else{
                const string=JSON.stringify(results);
                const json =  JSON.parse(string);
                // console.log(json[0].saldo);
                const nuevo_saldo = parseFloat(json[0].saldo) + saldo;

                conexion.query(`UPDATE cliente SET saldo = ${nuevo_saldo} WHERE id = ${id_usuario}`,(error_update, result_update) => {
                    if(error_update){
                        throw error_update;
                    }else{
                        conexion.query(`INSERT INTO log_transacciones (glosa_transaccion, id_cliente, id_producto, monto) VALUES ("Recarga", ${id_usuario}, "NULL", ${saldo})`, (error_log, result_log) => {
                            if(error_log){
                                throw error_log;
                            }else{
                                console.log(result_update);
                                res.send(result_update);
                            }
                        })
                    }
                });

            }
        })
    } catch (error) {
        throw error;        
    }
}

const getSaldoCliente = (req, res = response) => {
    const { id_usuario } = req.body;
    try {
        conexion.query(`SELECT saldo FROM cliente WHERE id = ${id_usuario}`, (error, results) => {
            if(error){
                throw error;
            }else{
                const str_saldo=JSON.stringify(results);
                const json_saldo = JSON.parse(str_saldo);

                return res.status(200).json({
                    "ok": true,
                    "saldo":json_saldo[0].saldo
                });
            }
        });
    } catch (error) {
        throw error;
    }
}

module.exports = {
    getUsuarios,
    actualizarSaldo,
    getSaldoCliente
}