const { Router } = require("express");
const router = Router();

const { getHistoricos } = require('../controllers/historicos');

router.get("/:id_cliente", [], getHistoricos);

module.exports = router;