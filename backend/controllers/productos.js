const response = require("express");
const { conexion } = require('../database/db');

const getProductos = async(req, res) => {
    await conexion.query('SELECT * FROM productos', (error, results) => {
        if(error){
            console.log(error);
        }else{
            const str_productos=JSON.stringify(results);
            const json_productos = JSON.parse(str_productos);

            return res.status(200).json({
                "ok": true,
                "productos":json_productos
            });
        }
    });
};


const getPrecioProducto = (id_producto) => {
    
};

module.exports = {
    getProductos,
    getPrecioProducto
}