const mysql = require('mysql');

const conexion = mysql.createConnection({
    host:  process.env.HOST,
    database: process.env.DATABASE_NAME,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS
});

conexion.connect(function(error){
    if(error){
        throw error;
    }else{
        console.log('CONEXION EXITOSA');
    }
});

module.exports = {
    conexion
}