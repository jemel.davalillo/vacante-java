const { Router } = require("express");
const router = Router();

const { getCompras, getComprasByCliente, comprarProducto } = require('../controllers/compras');

router.get("/", [], getCompras);
router.get("/:id_cliente", [], getComprasByCliente);
router.post("/comprar", [], comprarProducto);

module.exports = router;