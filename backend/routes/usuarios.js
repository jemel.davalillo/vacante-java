const { Router } = require("express");
const router = Router();

const { getUsuarios, actualizarSaldo, getSaldoCliente } = require('../controllers/usuarios');

router.get("/", [], getUsuarios);
router.get("/cliente/saldo", [], getSaldoCliente);
router. put("/cliente", [], actualizarSaldo );

module.exports = router;