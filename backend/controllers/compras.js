const response = require("express");
const { conexion } = require('../database/db');

const getCompras = async(req, res) => {
    const { id_cliente } = req.body;
    await conexion.query(`SELECT productos.descripcion,
                                productos.precio,
                                cliente.nombre_cliente,
                                producto_cliente.fecha_creacion as fecha_de_compra
                            FROM producto_cliente
                            JOIN cliente ON (producto_cliente.id_cliente = cliente.id)
                            JOIN productos ON (producto_cliente.id_producto = productos.id)
                        `, (error, results) => {
        if(error){
            console.log(error);
        }else{
           res.send(results);
        }
    });
};

const getComprasByCliente = async(req, res) => {
    const { id_cliente } = req.body;
    await conexion.query(`SELECT 
                                productos.descripcion,
                                productos.precio,
                                cliente.nombre_cliente,
                                producto_cliente.fecha_creacion as fecha_de_compra
                            FROM producto_cliente
                            JOIN cliente ON (producto_cliente.id_cliente = cliente.id)
                            JOIN productos ON (producto_cliente.id_producto = productos.id)
                            WHERE producto_cliente.id_cliente = ${id_cliente}
                        `, (error, results) => {
        if(error){
            console.log(error);
        }else{
           res.send(results);
        }
    });
};


const comprarProducto = async(req, res) => {
    const { id_producto, id_cliente } = req.body;
    
    conexion.query(`SELECT precio FROM productos WHERE id = ${id_producto}`,(error, result) => {
        if(error){
            throw error;
        }else{
            const string=JSON.stringify(result);
            const json =  JSON.parse(string);
            const precio_producto =  json[0].precio;

            //Se obtiene el saldo del cliente
            conexion.query(`SELECT saldo FROM cliente WHERE cliente.id = ${id_cliente} and cliente.activo`, (error_saldo, saldo) => {
                if(error_saldo){
                    throw error_saldo;
                }else{
                    //Se verifica que el saldo del cliente sea mayor al costo del producto
                    const string_saldo=JSON.stringify(saldo);
                    const json_saldo = JSON.parse(string_saldo);
                    const saldo_cliente = json_saldo[0].saldo;
                    if(saldo_cliente > precio_producto){
                        console.log('Saldo verificado.');
                        conexion.query(`INSERT INTO producto_cliente (id_producto, id_cliente) VALUES (${id_producto}, ${id_cliente})`,(error_compra, result_compra) => {
                            if(error_compra){
                                throw error_compra;
                            }else{
                                //Se actualiza el saldo del cliente
                                const nuevo_saldo_cliente = saldo_cliente - precio_producto;
                                conexion.query(`UPDATE cliente SET saldo = ${nuevo_saldo_cliente} WHERE id = ${id_cliente}`, (error_update, result_update) => {
                                    if(error_update){
                                        throw error_update;
                                    }else{
                                        conexion.query(`SELECT * FROM productos WHERE id = ${id_producto}`, (error_producto, result_producto) => {
                                            if(error_producto){
                                                throw error_producto;
                                            }else{
                                                const string_glosa_producto=JSON.stringify(result_producto);
                                                const json_producto = JSON.parse(string_glosa_producto);
                                                const glosa = json_producto[0].descripcion;
                                                const monto = json_producto[0].precio;

                                                conexion.query(`INSERT INTO log_transacciones (glosa_transaccion, id_cliente, id_producto, monto) VALUES ("${glosa}",${id_cliente},${id_producto},${monto})`, (error_log, result_log) => {
                                                    if(error_log){
                                                        throw error_log
                                                    }else{
                                                        res.status(200).json({
                                                            "ok": true,
                                                            "mensaje": "compra realizada"
                                                        });
                                                        console.log("Compra Realizada con éxito");
                                                    }
                                                });
                                            }
                                        });
                                        
                                    }
                                });
                            }
                        });
                    }else{
                        res.status(400).json({
                            "ok": false,
                            "mensaje": "Saldo insuficiente"
                        });
                        console.log('Saldo insuficiente');
                    }
                }
            });
        }
    });
    
}

module.exports = {
    getCompras,
    getComprasByCliente,
    comprarProducto
}